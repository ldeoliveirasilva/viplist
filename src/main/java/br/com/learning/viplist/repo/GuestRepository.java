package br.com.learning.viplist.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.learning.viplist.model.Guest;
@Repository
public interface GuestRepository extends CrudRepository<Guest, Integer> {
}
